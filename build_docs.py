#!/usr/bin/env python3
import mistune
import os
import bs4
import tortilla
import logging
import argparse
from requests.auth import HTTPBasicAuth

HAS_SSPI = True
try:
	from requests_negotiate_sspi import HttpNegotiateAuth
except:
	HAS_SSPI = False

HAS_GSSAPI = True
try:
	from requests_gssapi import HTTPSPNEGOAuth
except:
	HAS_GSSAPI = False

logging.basicConfig(level='INFO', format='%(levelname)s: %(message)s')

class BasicConfluence:
    def __init__(self, host, username, password):
        self.host = host
        self.username = username
        self.password = password

        if None in [self.username, self.password]:
            if HAS_SSPI:
                self.api = tortilla.wrap(self.host, auth=HttpNegotiateAuth())
                logging.debug("Using SSPI authentication")
            elif HAS_GSSAPI:
                self.api = tortilla.wrap(self.host, auth=HTTPSPNEGOAuth())
                logging.debug("Using GSSAPI authentication")
        else:
            self.api = tortilla.wrap(self.host, auth=HTTPBasicAuth(username=self.username, password=self.password))
            logging.debug("Ussing Basic authentication")

    def get_page(self, title=None, space_key=None, page_id=None):
        if page_id is not None:
            return self.api.content.get(page_id)
        elif title is not None:
            params = {'title': title}
            if space_key is not None:
                params['spaceKey'] = space_key

            response = self.api.content.get(params=params)

            try:
                # A search by title/space doesn't return full page objects, and since we don't support expansion in this implementation
                # just yet, we just retrieve the "full" page data using the page ID for the first search result
                return self.get_page(page_id=response.results[0].id)
            except IndexError:
                return None
        else:
            raise ValueError("At least one of title or page_id must not be None")
    
    def add_label(self, labels=None, title=None, space_key=None, page_id=None):
        page = self.get_page(title=title, space_key=space_key, page_id=page_id)
        if page:
            logging.debug(f"Adding labels: {labels}")
            confluence_labels = [{'prefix': 'global', 'name': x} for x in labels]
            return self.api.content(page.id).label.post(data=confluence_labels)['results']

    def create_page(self, space, title, body, parent_id=None, update_message=None):
        page_structure = {
            'title': title,
            'type': 'page',
            'space': {
                'key': space
            },
            'body': {
                'storage': {
                    'value': body,
                    'representation': 'storage'
                }
            }
        }

        if parent_id is not None:
            if type(parent_id) is str:
                parent_id = int(parent_id)
            page_structure['ancestors'] = [{'id': parent_id}]

        if update_message is not None:
            page_structure['version'] = {'message': update_message}

        return self.api.content.post(json=page_structure)

    def update_page(self, page, body, update_message=None):
        update_structure = {
            'version': {
                'number': page.version.number + 1,
            },
            'title': page.title,
            'type': 'page',
            'body': {
                'storage': {
                    'value': body,
                    'representation': 'storage'
                }
            }
        }

        if update_message is not None:
            update_structure['version']['message'] = update_message

        return self.api.content.put(page.id, json=update_structure)
    
    def get_space_key(self, space_name, space_type=None):
        try:
            if space_type:
                logging.debug(f"Looking for {space_type} space by name: {space_name}")
                return [x['key'] for x in self.api.space.get(params={'type': space_type})['results'] if x['name'] == space_name][0]
            else:
                try:
                    logging.debug(f"Looking for space by name: {space_name}")
                    return [x['key'] for x in self.api.space.get()['results'] if x['name'] == space_name][0]
                except:
                    logging.debug(f"Looking for personal space by name: {space_name}")
                    return [x['key'] for x in self.api.space.get(params={'type': 'personal'})['results'] if x['name'] == space_name][0]
        except:
            logging.error(f"Unable to find space by name '{space_name}'")
            return

    def get_space_name(self, space_key, space_type=None):
        try:
            if space_type:
                logging.debug(f"Looking for {space_type} space by key: {space_key}")
                return [x['name'] for x in self.api.space.get(params={'type': space_type})['results'] if x['key'] == space_key][0]
            else:
                try:
                    logging.debug(f"Looking for space by key: {space_key}")
                    return [x['name'] for x in self.api.space.get()['results'] if x['key'] == space_key][0]
                except:
                    logging.debug(f"Looking for personal space by key: {space_key}")
                    return [x['name'] for x in self.api.space.get(params={'type': 'personal'})['results'] if x['key'] == space_key][0]
        except Exception as e:
            logging.error(f"Unable to find space by key '{space_key}': {e}")
            return
    
    def get_current_user_space(self):
        try:
            user = self.api.user.current.get(params={'expand': 'personalSpace'})
            if 'personalSpace' in user:
                return user['personalSpace']
            else:
                space_key = f"~{user['username']}"
                return {
                    'key': space_key,
                    'name': self.get_space_name(space_key, 'personal')
                }
        except:
            return

class ConfluenceTag(object):
    def __init__(self, name, text='', attrib=None, namespace='ac', cdata=False):
        self.name = name
        self.text = text
        self.namespace = namespace
        if attrib is None:
            attrib = {}
        self.attrib = attrib
        self.children = []
        self.cdata = cdata

    def render(self):
        namespaced_name = self.add_namespace(self.name, namespace=self.namespace)
        namespaced_attribs = {
            self.add_namespace(attribute_name, namespace=self.namespace): attribute_value
            for attribute_name, attribute_value in self.attrib.items()
        }

        content = '<{}{}>{}{}</{}>'.format(
            namespaced_name,
            ' {}'.format(' '.join(['{}="{}"'.format(name, value) for name, value in sorted(namespaced_attribs.items())])) if namespaced_attribs else '',
            ''.join([child.render() for child in self.children]),
            '<![CDATA[{}]]>'.format(self.text) if self.cdata else self.text,
            namespaced_name
        )
        return '{}\n'.format(content)

    @staticmethod
    def add_namespace(tag, namespace):
        return '{}:{}'.format(namespace, tag)

    def append(self, child):
        self.children.append(child)

class ConfluenceRenderer(mistune.Renderer):
    def structured_macro(self, name):
        return ConfluenceTag('structured-macro', attrib={'name': name})

    def parameter(self, name, value):
        parameter_tag = ConfluenceTag('parameter', attrib={'name': name})
        parameter_tag.text = value
        return parameter_tag

    def plain_text_body(self, text):
        body_tag = ConfluenceTag('plain-text-body', cdata=True)
        body_tag.text = text
        return body_tag

    def block_code(self, code, lang=None):
        root_element = self.structured_macro('code')
        if lang is not None:
            lang_parameter = self.parameter(name='language', value=lang)
            root_element.append(lang_parameter)
        root_element.append(self.parameter(name='linenumbers', value='false'))
        root_element.append(self.parameter(name='theme', value='RDark'))
        root_element.append(self.plain_text_body(code))
        return root_element.render()

    def image(self, src, title, text):
        attributes = {'alt': text}
        if title:
            attributes['title'] = title

        root_element = ConfluenceTag(name='image', attrib=attributes)
        url_tag = ConfluenceTag('url', attrib={'value': src}, namespace='ri')
        root_element.append(url_tag)

        return root_element.render()

def markdown_to_confluence(markdown_file, add_note=False):
    with open(markdown_file, 'r') as f:
        markdown_data = ''
        labels = []
        space_key = ''
        space_name = ''
        parent = None
        add_note_next_line = False
        
        # build page title from first H1 header
        html_mistune = mistune.Markdown()
        html_content = html_mistune(f.read())
        f.seek(0, os.SEEK_SET)

        soup = bs4.BeautifulSoup(html_content, features='lxml')
        try:
            page_title = soup.h1.text
            soup.h1.decompose()
        except AttributeError:
            page_title = os.path.splitext(os.path.basename(markdown_file))[0]
        
        # build markdown data to pass to converter, pulling out comment directives
        for line in f:
            # if lines start with weird comment syntax, look for cf-directives
            if line.startswith('[//]'):
                k, v = line[9:-2].split(': ')
                if k.strip() == 'cf-labels':
                    labels = v.strip().split(' ')
                if k.strip() == 'cf-space':
                    space_name = v.strip()
                    logging.debug(f"Searching for space '{space_name}'")
                    space_key = confluence.get_space_key(space_name)
                    logging.debug(f"Found space key '{space_key}'")
                if space_key and k.strip() == 'cf-parent':
                    parent = confluence.get_page(title=v.strip(), space_key=space_key)
                    if parent:
                        parent = parent.id
            elif line.startswith('# '):
                if add_note:
                    add_note_next_line = True
                # don't print h1 since it should already be the page title
                # markdown_data += line
            elif add_note_next_line:
                # add note about source control updating
                markdown_data += '\n<span style="color:blue">NOTE: This document is automatically updated from Bitbucket</span>\n'
                markdown_data += line
                add_note = False
                add_note_next_line = False
            else:
                markdown_data += line

    # convert markdown
    renderer = ConfluenceRenderer(use_xhtml=True)
    confluence_mistune = mistune.Markdown(renderer=renderer)
    confluence_body = confluence_mistune(markdown_data)

    return {
        'title': page_title,
        'body': confluence_body,
        'labels': labels,
        'space_name': space_name,
        'space_key': space_key,
        'parent': parent
    }

if __name__ == "__main__":
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('markdown_file', nargs='+')
        parser.add_argument('--host', default=os.getenv('CONFLUENCE_HOST'), help="Confluence REST API url")
        parser.add_argument('--space', default=os.getenv('CONFLUENCE_SPACE'), help="Confluence Space key")
        parser.add_argument('--username', default=os.getenv('CONFLUENCE_USERNAME'), help="Confluence username")
        parser.add_argument('--password', default=os.getenv('CONFLUENCE_PASSWORD'), help="Confluence password")
        parser.add_argument('--add_note', action='store_true', help='Add note on each page that is automatically updated from source control')
        args = parser.parse_args()

        # get confluence connection details from environment
        if not args.host:
            logging.error("CONFLUENCE_HOST environment variable is empty or missing")
            exit(1)
        
        confluence_host = args.host
        confluence_space = args.space
        confluence_user = args.username
        confluence_password = args.password

        # get basic confluence api class
        confluence = BasicConfluence(host=confluence_host, username=confluence_user, password=confluence_password)

        if confluence_space:
            default_space = {
                'key': confluence_space,
                'name': confluence.get_space_name(confluence_space)
            }
        else:
            # get the current user's space in case a document doesn't specify its own
            default_space = confluence.get_current_user_space()
        
        for md_file in args.markdown_file:
            if os.path.isfile(md_file):
                
                # convert markdown files to confluence markup/storage
                confluence_body = markdown_to_confluence(md_file, add_note=args.add_note)
                space_name = confluence_body['space_name'] if confluence_body['space_name'] else default_space['name']
                space_key = confluence_body['space_key'] if confluence_body['space_key'] else default_space['key']
                if confluence_body['body']:
                    
                    # look for existing page and udpate if found, otherwise create new page
                    page = confluence.get_page(space_key=space_key, title=confluence_body['title'])
                    if page:
                        logging.info(f"Updating page {confluence_body['title']} in {space_name} space")
                        page = confluence.update_page(page=page, body=confluence_body['body'])
                    else:
                        logging.info(f"Creating page {confluence_body['title']} in {space_name} space")
                        page = confluence.create_page(space=space_key, parent_id=confluence_body['parent'], title=confluence_body['title'], body=confluence_body['body'])
                    
                    # if a labels directive was found in the file, add the labels to the page
                    if confluence_body['labels']:
                        confluence.add_label(labels=confluence_body['labels'], page_id=page.id)
                else:
                    logging.error(f"Unable to parse '{md_file}'")
            else:
                logging.error(f"Unable to find '{md_file}'")
    except Exception as e:
        logging.error(e)
        exit(1)
