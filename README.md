[//]: # (cf-space: Michael Charo)
[//]: # (cf-labels: python howto)

# Python Markdown to Confluence Uploader

## Usage

### Bash

```bash
## gather markdown files in current directory
markdowns=($(find . -type f -name '*.md'))

## basic usage
./build_docs.py --host https://charo.atlassian.net/rest/api $markdowns

## add note to the top of the page indicating it is automatically updated from Bitbucket
./build_docs.py --host https://charo.atlassian.net/rest/api --add_note $markdowns
```

### PowerShell

```powershell
## gather markdown files in current directory
$Markdown = Get-ChildItem -Recurse -Filter *.md | Select-Object -ExpandProperty FullName

## basic usage
./build_docs.py --host https://charo.atlassian.net/rest/api $Markdown

## add note to the top of the page indicating it is automatically updated from Bitbucket
./build_docs.py --host https://charo.atlassian.net/rest/api --add_note $Markdown
```

## Features

- Converts Markdown to Confluence storage format and creates or updates a Confluence page
- Parses optional comments (see comment format below):
  - cf-space - The space name for the page
  - cf-labels - A space separated list of labels to attach to the page
  - cf-parent - The title of the parent page for this document

## Markdown Comment Format

- Must be the first lines of the document
- Must follow the exact format below.  This format isn't very friendly but it's valid and invisibly-rendered Markdown:
  - `[//]: # (cf-space: Michael Charo)`
